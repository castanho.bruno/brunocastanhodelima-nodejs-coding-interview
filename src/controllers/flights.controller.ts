import { JsonController, Get } from 'routing-controllers'
import { FlightsService } from '../services/flights.service'

const flightsService = new FlightsService()

@JsonController('/flights')
export default class FlightsController {
    @Get('', { transformResponse: false })
    async getAll() {
        const data = await flightsService.getAll()
        return {
            status: 200,
            data,
        }
    }

    // POST '/v1/flights/{flightId}'
    // Body
    // ```
    //     {
    //         "id": "62aa470e3ff76539462829a1"
    //     }
    // ```
}
