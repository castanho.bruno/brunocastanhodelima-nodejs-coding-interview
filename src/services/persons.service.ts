import { PersonsModel } from '../models/persons.model'

export class PersonsService {
    getAll() {
        return PersonsModel.find()
    }

    async create(person: any) {
        PersonsModel.create(person)
    }
}
